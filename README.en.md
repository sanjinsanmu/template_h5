<!--
 * @Date: 2023-04-24 10:26:42
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 17:21:29
 * @LastEditors: *Sen
-->

## 安装

约定只能用 [pnpm](https://pnpm.io/) 管理项目依赖。

命令

```bash
$ pnpm i
```

将安装所有依赖。

## 预览

命令

```bash
$ pnpm dev/pro/test
```

将启动站点供您预览已开发的页面组件。

## 打包

命令

```bash
$ pnpm build:dev/pro/test
```

### 避免 v-if 和 v-for 用在一起

**永远不要把 `v-if` 和 `v-for` 同时用在同一个元素上。**

一般我们在两种常见的情况下会倾向于这样做：

为了过滤一个列表中的项目 (比如 v-for="user in users" v-if="user.isActive")。在这种情形下，请将 users 替换为一个计算属性 (比如 activeUsers)，让其返回过滤后的列表。

为了避免渲染本应该被隐藏的列表 (比如 v-for="user in users" v-if="shouldShowUsers")。这种情形下，请将 v-if 移动至容器元素上 (比如 ul、ol)。

### 为组件样式设置作用域

**对于应用来说，顶级 App 组件和布局组件中的样式可以是全局的，但是其它所有组件都应该是有作用域的。**

### 单文件组件文件名的大小写

单文件组件的文件名应该要么始终是单词大写开头 (PascalCase)，要么始终是横线连接 (kebab-case)。

components/
|- MyComponent.vue

components/
|- my-component.vue

### 完整单词的组件名

**组件名应该倾向于完整单词而不是缩写。**

编辑器中的自动补全已经让书写长命名的代价非常之低了，而其带来的明确性却是非常宝贵的。不常用的缩写尤其应该避免。

### 简单的计算属性

**应该把复杂计算属性分割为尽可能多的更简单的 property。**¡
