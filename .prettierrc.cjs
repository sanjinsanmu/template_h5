/*
 * @Date: 2023-05-21 15:23:51
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:22:34
 * @LastEditors: *Sen
 */
/** .prettierrc.js
 * 在VSCode中安装prettier插件 打开插件配置填写`.prettierrc.js` 将本文件作为其代码格式化规范
 * 在本文件中修改格式化规则，不会同时触发改变ESLint代码检查，所以每次修改本文件需要重启VSCode，ESLint检查才能同步代码格式化
 * 需要相应的代码格式化规范请自行查阅配置，下面为默认项目配置
 */
module.exports = {
  printWidth: 120,
  tabWidth: 2,
  endOfLine: "auto",
  useTabs: false,
  singleQuote: true,
  semi: false,
  arrowParens: "avoid",
  jsxSingleQuote: true,
  trailingComma: "none",
  bracketSpacing: true,
  jsxBracketSameLine: true,
  eslintIntegration: true,
};
