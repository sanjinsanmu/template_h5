/*
 * @Date: 2023-04-24 10:26:42
 * @Author: *Sen
 * @Description: vite配置
 * @LastEditTime: 2024-01-12 14:36:48
 * @LastEditors: *Sen
 */
import { defineConfig, loadEnv } from 'vite'
import postCssPxToRem from 'postcss-pxtorem'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import eslintPlugin from 'vite-plugin-eslint'

//自动引入
import AutoImport from 'unplugin-auto-import/vite'

import Components from 'unplugin-vue-components/vite'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import { VantResolver } from '@vant/auto-import-resolver'

export default ({ command, mode, ssrBuild }) => {
  const env = loadEnv(mode, process.cwd())
  console.log(' → >  → >  → >  *** *** *** ↓↓↓ (* ^ env ^ *) ↓↓↓ *** *** ***  < ←  < ←  < ← :', '\n', env)
  const htmlPlugin = () => {
    return {
      name: 'html-transform',
      transformIndexHtml(html) {
        return html.replace(/<title>(.*?)<\/title>/, `<title>${env.VITE_VUE_APP_TITLE}</title>`)
      }
    }
  }
  return defineConfig({
    base: env.VITE_VUE_ROUTER_BASE,
    plugins: [
      vue(),
      AutoImport({
        imports: ['vue', 'vue-router', 'pinia'],
        resolvers: [
          VantResolver(),
          IconsResolver({
            prefix: 'Icon'
          })
        ],
        eslintrc: {
          enabled: true // 1、改为true用于生成eslint配置。2、生成后改回false，避免重复生成消耗
        },
        dts: 'src/automation/auto-imports.d.ts'
      }),
      Components({
        resolvers: [
          VantResolver(),
          IconsResolver({
            enabledCollections: ['ep']
          })
        ],
        dts: 'src/automation/vue-components.d.ts'
      }),
      Icons({
        autoInstall: true
      }),
      eslintPlugin({
        include: ['src/**/*.ts', 'src/**/*.vue', 'src/*.ts', 'src/*.vue', 'src/**/*.tsx', 'src/*.tsx']
      }),
      htmlPlugin()
    ],
    // ↓解析配置
    resolve: {
      // ↓路径别名
      alias: {
        '@': resolve(__dirname, './src'),
        '@src': resolve(__dirname, './src'),
        '@assets': resolve(__dirname, './src/assets'),
        '@components': resolve(__dirname, './src/components'),
        '@router': resolve(__dirname, './src/router'),
        '@store': resolve(__dirname, './src/store'),
        '@types': resolve(__dirname, './src/types'),
        '@pages': resolve(__dirname, './src/pages')
      }
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@src/assets/style/variables.scss";'
        }
      },
      // 配置 CSS modules 的行为。选项将被传递给 postcss-modules
      modules: {},
      // 内联的 PostCSS 配置（格式同 postcss.config.js）
      postcss: {
        plugins: [
          postCssPxToRem({
            rootValue: 75, // 1rem 的大小
            propList: ['*'], // 需要转换的属性，*(全部转换)
            unitPrecision: 10, // 转换精度，保留的小数位数
            // selectorBlackList: ['.van']
          })
        ]
      }
    },
    server: {
      host: '0.0.0.0', // 本地启动的地址
      port: 8888, // 服务端口号
      open: true
      // proxy: { // 代理配置
      //   '/api/': {
      //     target: 'http:xxx.xxx.xx.x',
      //     rewrite: (path) => path.replace(/^\/api/, '')
      //   }
      // },
    },
    build: {
      sourcemap: false,
      outDir: env.VITE_VUE_APP_BUILD_OUT_NAME,
      minify: 'terser', // 构建时的压缩方式
      terserOptions: {
        compress: {
          drop_console: true,
          drop_debugger: true
        }
      },
      assetsDir: 'static',
      chunkSizeWarningLimit: 1024,
      rollupOptions: {
        output: {
          manualChunks(id) {
            if (id.includes('/node_modules/') || id.includes('/packages/')) {
              const modules = [
                'vant',
                'vue-router',
                'vue',
                'axios',
                'echarts',
                'pinia-plugin-persistedstate',
                'pinia',
                'qs'
              ]
              const chunk = modules.find(
                module => id.includes(`/node_modules/${module}`) || id.includes(`/packages/${module}`)
              )
              return chunk ? `vendor-${chunk}` : 'vendor'
            }
          }
        }
      }
    }
  })
}
