/*
 * @Date: 2023-05-21 14:17:15
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:29:28
 * @LastEditors: *Sen
 */
/**
 * @description: 导出excel 根据数据流
 * @param {*} fileData  表格数据（Array）
 * @param {*} xlsxName  导出表格名称
 * @return {*}
 * @author: Xsen
 */
const exportStreamExcel = (fileData, xlsxName) => {
  const blob = new Blob([fileData], { type: 'application/x-xls' })
  if ('download' in document.createElement('a')) {
    // 非IE下载
    const elink = document.createElement('a')
    elink.download = xlsxName + '.xlsx'
    elink.style.display = 'none'
    elink.href = URL.createObjectURL(blob)
    document.body.appendChild(elink)
    elink.click()
    URL.revokeObjectURL(elink.href) // 释放URL 对象
    document.body.removeChild(elink)
  }
}

export default exportStreamExcel
