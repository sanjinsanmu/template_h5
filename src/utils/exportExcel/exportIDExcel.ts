/*
 * @Date: 2023-05-21 14:17:05
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:23:09
 * @LastEditors: *Sen
 */
import FileSaver from 'file-saver'
import XLSX from 'xlsx'

/**
 * @description: 导出excel 根据ID
 * @param {*} ID 表格id
 * @param {*} xlsxName 导出的表格名称
 * @return {*}
 * @author: Xsen
 */
const exportIDExcel = (ID, xlsxName) => {
  const wb = XLSX.utils.table_to_book(document.querySelector(`#${ID}`))
  const wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'array' })
  try {
    FileSaver.saveAs(new Blob([wbout], { type: 'application/octet-stream' }), `${xlsxName}.xlsx`)
  } catch (e) {
    if (typeof console !== 'undefined') console.log(e, wbout)
  }
  return wbout
}

export default exportIDExcel
