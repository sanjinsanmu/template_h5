/*
 * @Date: 2023-05-05 10:29:08
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-22 16:54:32
 * @LastEditors: *Sen
 */
import axios from '@/utils/service/index'
import qs from 'qs'
namespace Login {
  // 用户登录表单
  export interface LoginReqForm {
    username: string
    password: string
  }
  // 登录成功后返回的token
  export interface LoginResData {
    token: any
  }
}

// 用户登录 params不做校验也行
export const login: any = (params: Login.LoginReqForm) => {
  return axios.post(`${import.meta.env.VITE_APP_SPDDataService}/auth/token`, params)
}
