/*
 * @Date: 2023-05-17 14:50:57
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-22 16:45:59
 * @LastEditors: *Sen
 */
import { AddRouteRecordRaw } from '../index'

export default [
  {
    path: '/',
    name: 'login',
    hidden: false, // 自定义添加的属性
    meta: {
      title: '登录'
    },
    component: () => import('@/pages/login/login.vue')
  },
  {
    path: '/:pathMatch(.*)*', // 或者 /:pathMatch(.*)*
    name: '404',
    hidden: false, // 自定义添加的属性
    meta: {
      title: '404'
    },
    component: () => import('@/pages/login/Error404.vue')
  }
] as AddRouteRecordRaw[]
