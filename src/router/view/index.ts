/*
 * @Date: 2023-05-17 14:52:02
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2024-01-03 16:58:34
 * @LastEditors: *Sen
 */
import { AddRouteRecordRaw } from '../index'

export default [
  {
    path: '/',
    name: 'layout',
    component: () => import('@/layout/Index.vue'),
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'home',
        component: () => import('@/pages/home/Index.vue')
      }
    ]
  }
] as AddRouteRecordRaw[]
