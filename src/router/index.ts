/*
 * @Date: 2023-04-24 10:58:33
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:30:54
 * @LastEditors: *Sen
 */
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import viewsRouter from './view'
import pageRouter from './pages'

// 对RouteRecordRaw类型进行扩展
export type AddRouteRecordRaw = RouteRecordRaw & {
  hidden?: boolean
}

const router = createRouter({
  history: createWebHashHistory(),
  routes: [...viewsRouter, ...pageRouter] as AddRouteRecordRaw[]
})

export default router
