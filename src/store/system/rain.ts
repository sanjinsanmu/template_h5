/*
 * @Date: 2023-04-24 11:49:09
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:30:08
 * @LastEditors: *Sen
 */
import { acceptHMRUpdate, defineStore } from 'pinia'
export const useRainStore = defineStore('rain', {
  state: () => ({}),
  getters: {},
  actions: {}
})
