/*
 * @Date: 2023-04-24 11:41:06
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:30:04
 * @LastEditors: *Sen
 */
import { Pinia } from 'pinia'
import { useRainStore } from './rain'

export const systemStore = {
  useRainStore: (pinia?: Pinia) => useRainStore(pinia)
}
