/*
 * @Date: 2023-04-24 11:39:41
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:29:56
 * @LastEditors: *Sen
 */
import { Pinia } from 'pinia'
import { useGlobalStore } from './user'

export const gloablStore = {
  useGlobalStore: (pinia?: Pinia) => useGlobalStore(pinia)
}
