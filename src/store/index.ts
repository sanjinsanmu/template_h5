/*
 * @Date: 2023-04-24 10:56:49
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:30:10
 * @LastEditors: *Sen
 */

import { createPinia } from 'pinia'
import { gloablStore } from './global/index'
import { systemStore } from './system/index'

export const pinia = createPinia()

/**
 * 项目全局 store。
 * 组件内使用不需要传 pinia，组件外使用需要传 pinia。
 */
export const store = {
  global: {
    ...gloablStore
  },
  system: {
    ...systemStore
  }
}
