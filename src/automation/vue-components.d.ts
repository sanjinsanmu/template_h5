/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    Index: typeof import('./../components/home-header-area/Index.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    VanCol: typeof import('vant/es')['Col']
    VanIcon: typeof import('vant/es')['Icon']
    VanRow: typeof import('vant/es')['Row']
    VanSearch: typeof import('vant/es')['Search']
    VanSwipe: typeof import('vant/es')['Swipe']
    VanSwipeItem: typeof import('vant/es')['SwipeItem']
    VanTab: typeof import('vant/es')['Tab']
    VanTabbar: typeof import('vant/es')['Tabbar']
    VanTabbarItem: typeof import('vant/es')['TabbarItem']
    VanTabs: typeof import('vant/es')['Tabs']
  }
}
