/*
 * @Date: 2023-04-24 10:26:42
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2024-01-04 10:49:19
 * @LastEditors: *Sen
 */
import 'amfe-flexible'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'
import { pinia } from '@/store/index'

const app = createApp(App)
app.use(pinia)
app.use(router)
app.mount('#app')
