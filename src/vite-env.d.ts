/*
 * @Date: 2023-04-24 10:26:42
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-21 16:30:30
 * @LastEditors: *Sen
 */
/// <reference types="vite/client" />

// declare module '*.vue' {
//   import { DefineComponent } from 'vue'
//   // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
//   const component: DefineComponent<{}, {}, any>
//   export default component
// }

declare module '*.vue' {
  import { ComponentOptions } from 'vue'
  const componentOptions: ComponentOptions
  export default componentOptions
}
