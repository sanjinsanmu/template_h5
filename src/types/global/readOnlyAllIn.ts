/*
 * @Date: 2023-05-22 17:03:38
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-05-22 17:06:12
 * @LastEditors: *Sen
 */

export interface readOnlyAllIn {
  readonly [propName: string]: any
}
