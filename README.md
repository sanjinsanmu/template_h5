<!--
 * @Date: 2023-04-24 10:26:42
 * @Author: *Sen
 * @Description: *****
 * @LastEditTime: 2023-12-26 18:38:31
 * @LastEditors: *Sen
-->

## 安装

约定只能用 [pnpm](https://pnpm.io/) 管理项目依赖。

命令

```bash
$ pnpm i
```

将安装所有依赖。

## 预览

命令

```bash
$ pnpm dev/pro/test
```

将启动站点供您预览已开发的页面组件。

## 打包

命令

```bash
$ pnpm build:dev/pro/test
```

### 安装完依赖后重新打开项目（不是在 vscode 内置终端下载的跳过）
